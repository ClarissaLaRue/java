package ru.nsu.fit.korobova;

import java.util.Map;

public class ConditionProcessor implements TemplateProcessor {
    private StringBuilder block;
    private StringBuilder content;
    private StringBuilder condition;

    public ConditionProcessor(StringBuilder string, StringBuilder condition){
        block = new StringBuilder();
        content = new StringBuilder(string);
        this.condition = new StringBuilder(condition);
    }

    @Override
    public StringBuilder getString() {
        return block;
    }

    @Override
    public void fillTemplate(Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (conditions.isEmpty()){
            throw new ArgumentNotFoundException("Not found condition");
        }
        if (!conditions.containsKey(new String(condition))){//вставка content если есть
            throw new ArgumentNotFoundException("Not found condition");
        }else{
            if (conditions.get(new String(condition))){
                block = new StringBuilder(content);
            }
        }
    }
}
