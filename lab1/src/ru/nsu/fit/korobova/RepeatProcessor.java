package ru.nsu.fit.korobova;

import java.util.Map;

public class RepeatProcessor implements TemplateProcessor {
    private StringBuilder block;
    private StringBuilder content;
    private StringBuilder iteration;


    private static final String STARTREPEAT= "!repeat ";

    public RepeatProcessor(StringBuilder string, StringBuilder iteration){
        block = new StringBuilder();
        content = new StringBuilder(string);
        this.iteration = new StringBuilder(iteration);
    }

    @Override
    public StringBuilder getString() {
        return block;
    }

    @Override
    public void fillTemplate(Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (iterations.isEmpty()){
            throw new ArgumentNotFoundException("Not found iteration");
        }
        if (!iterations.containsKey(new String(iteration))){//вставка content если есть
            throw new ArgumentNotFoundException("Not found iteration");
        }else{
            for(int i =0; i<iterations.get(new String(iteration)); i++){
                block.append(content);
            }
        }
    }
}
