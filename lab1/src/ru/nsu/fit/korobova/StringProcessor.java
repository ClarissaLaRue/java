package ru.nsu.fit.korobova;

import java.util.Map;

public class StringProcessor implements TemplateProcessor {
    private StringBuilder block;

    public StringProcessor(StringBuilder string){
        block = new StringBuilder(string);
    }

    @Override
    public StringBuilder getString() {
        return block;
    }

    @Override
    public void fillTemplate(Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) {

    }
}
