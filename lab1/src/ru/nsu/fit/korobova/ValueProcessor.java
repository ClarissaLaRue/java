package ru.nsu.fit.korobova;

import java.util.Map;

public class ValueProcessor implements TemplateProcessor{
    private StringBuilder block;

    private static final String NAME= "name";

    ValueProcessor(){};

    @Override
    public StringBuilder getString() {
        return block;
    }

    @Override
    public void fillTemplate(Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (values.get(NAME) == null){
            throw new ArgumentNotFoundException("Not found name");
        }else{
            block = new StringBuilder(values.get(NAME));
        }
    }
}
