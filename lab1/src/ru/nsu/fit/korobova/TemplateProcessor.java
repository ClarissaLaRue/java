package ru.nsu.fit.korobova;

import java.util.Map;

public interface TemplateProcessor {
    StringBuilder getString();
    void fillTemplate(Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException;
}


