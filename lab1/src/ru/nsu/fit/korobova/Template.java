package ru.nsu.fit.korobova;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Template {

    private final List<TemplateProcessor> processor = new ArrayList<>();

    private static final int STRING=0;
    private static final int VALUE=1;
    private static final int CONDITION=2;
    private static final int REPEAT=3;

    private static final String NAME= "name";
    private static final String STRATIF = "!if ";
    private static final String ENDIF = "!endif!";
    private static final String STARTREPEAT= "!repeat ";
    private static final String ENDREPEAT = "!endrepeat!";

    private boolean isFirst = true;

    public Template(String s) throws ArgumentNotFoundException {
        String[] strings = s.split("(?<!\\\\)%"); //Разбиение по %, но /% игнорируется
        int proc = 0;
        for (int i = 0; i < strings.length; i++){
            if (strings[i].contains("\\%")){
                processor.add( new StringProcessor(insertPercent(strings[i])));
                proc++;
            }
            if (strings[i].equals(NAME)){
                processor.add( new ValueProcessor());
                proc++;
            }
            if (strings[i].contains(STRATIF)){
                StringBuilder contdition = new StringBuilder(
                        strings[i].substring(strings[i].indexOf(STRATIF)+STRATIF.length(), strings[i].length()-1));
                processor.add( new ConditionProcessor(insertIf(strings, i), contdition));
                i+=2;
                proc++;
            }else if (strings[i].contains(STARTREPEAT)){
                StringBuilder iteration = new StringBuilder(
                        strings[i].substring(strings[i].indexOf(STARTREPEAT)+STARTREPEAT.length(), strings[i].length()-1));
                processor.add( new RepeatProcessor(insertRepeat(strings, i), iteration));
                i+=2;
                proc++;
            }
            if (proc == 0){
                processor.add( new StringProcessor(new StringBuilder(strings[i])));
            }
            proc = 0;
        }
        //находим подстроку, ее конец и делим так строку
        //Создаем блоки
    }

     public String fill(Map<String, String> values, Map<String, Boolean> conditions, Map<String, Integer> iterations) throws ArgumentNotFoundException {
        StringBuilder templateString = new StringBuilder();
        for (int i =0; i< processor.size(); i++){ //Проходим по списку процессоров
            processor.get(i).fillTemplate(values, conditions, iterations); //Вызываем нужный процесс
            templateString.append(processor.get(i).getString());

        }
        String fillString = new String(templateString);
        return fillString;
     }


    //Заменяем в строке /% на % и вставляем в список
    private StringBuilder insertPercent (String string){
        StringBuilder newBlock = new StringBuilder();
        newBlock.append(string.substring(0, string.indexOf("\\%")));
        newBlock.append("%");
        newBlock.append(string.substring(string.indexOf("\\%")+"\\%".length(), string.length()));
        while (newBlock.indexOf("\\%") != -1 ){
            StringBuilder step = new StringBuilder();
            step.append(newBlock.substring(0, newBlock.indexOf("\\%")));
            step.append("%");
            step.append(newBlock.substring(newBlock.indexOf("\\%")+"\\%".length(), newBlock.length()));
            newBlock.delete(0, newBlock.length());
            newBlock = step;
        }
        return newBlock;
    }

    //Обработка шаблона IF
    private StringBuilder insertIf(String[] strings, int startIf) throws ArgumentNotFoundException {
        StringBuilder ifString = new StringBuilder();
        //проверяем следующие 2 позиции
        //Start If, Contant(не может разделяться /%), End If
        if (startIf == strings.length -1){
            throw new ArgumentNotFoundException("Not found engif");
        }
        int pos = startIf + 1;
        if (pos == strings.length){
            throw new ArgumentNotFoundException("Not found engif");
        }
        if (pos < strings.length){ //content of end if without content
            //либо пустая строка, либо Contant
            if (strings[pos].contains("/%")){
                ifString.append(insertPercent(strings[pos]));
            }else {
                ifString.append(strings[pos]);
            }
        }
        pos++;
        if (pos < strings.length){//end if или неожиданная команда
            if ((strings[pos].equals(NAME)) || (strings[pos].contains(STARTREPEAT)) || (strings[pos].contains(ENDREPEAT))){
                throw new ArgumentNotFoundException("Unexpected instruction after if");
            } else if(strings[pos].equals(ENDIF)){
                return ifString;
            }
        }
        return ifString;
    }

    private StringBuilder insertRepeat(String [] strings, int startRepeat) throws ArgumentNotFoundException {
        StringBuilder repeatString = new StringBuilder();
        int pos = startRepeat + 1;
        if (pos == strings.length){
            throw new ArgumentNotFoundException("Not found engif");
        }
        if (pos < strings.length){
            //либо пустая строка, либо Contant
            if (strings[pos].contains("/%")){
                repeatString.append(insertPercent(strings[pos]));
            }else {
                repeatString.append(strings[pos]);
            }
        }
        pos++;
        if (pos < strings.length){//end if или неожиданная команда
            if ((strings[pos].equals(NAME)) || (strings[pos].contains(STRATIF)) || (strings[pos].contains(ENDIF))){
                throw new ArgumentNotFoundException("Unexpected instruction after repeat");
            } else if(strings[pos].equals(ENDREPEAT)){
                return repeatString;
            }
        }
        return repeatString;
    }
}
