package ru.nsu.fit.korobova;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class main {
    public static void main(String[] args) {
        try {
            Template t = new Template( " H\\%ell\\%o %name%! %!if isUser!% Welcome back %!endif!% %!repeat 2!% doroy %!endrepeat!%");
            Map<String, String> values = new HashMap<>();
            values.put("name", "Vasya");
            Map<String, Boolean> conditions = new HashMap<>();
            conditions.put("isUser", true);
            Map<String, Integer> iterations = new HashMap<>();
            iterations.put("2", 2);
            System.out.println(t.fill(values, conditions, iterations));
        } catch (ArgumentNotFoundException e) {
            e.printStackTrace();
        }
    }
}
