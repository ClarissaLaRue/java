package ru.nsu.fit.korobova;

public class ArgumentNotFoundException extends Exception{
    public ArgumentNotFoundException(String message) {
        super(message);
    }
}

