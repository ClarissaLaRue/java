package ru.nsu.fit.korobova;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private int port;
    RequestHandler requestHandler = new RequestHandler();
    GetProcessor getProcessor = new GetProcessor();
    ResponseSender responseSender = new ResponseSender();

    public Server(int p) {
        port = p;
    }

    public void start () throws IOException, ServerException {
        ServerSocket serverSocket;
        try{
            serverSocket = new ServerSocket(port);
        }
        catch (IOException ex){
            throw new IOException();
        }
        Socket socket;

        while (true) {
            socket = serverSocket.accept();//Получаем Request
            InputStream in = socket.getInputStream();
            Request request = new Request();
            request = requestHandler.parseRequest(in); //Парсим Request
            Response response = new Response();//Получаем Response
            response = getProcessor.process(request);
            responseSender.setSocket(socket);
            responseSender.setResponse(response);
            responseSender.sendResponse();
        }

    }

}
