package ru.nsu.fit.korobova;

public class ServerException extends Exception {
    private int errorCode;
    private String message;

    ServerException(int code, String mes){
        errorCode = code;
        message = mes;
    }

    int GetErrorCode(){
        return errorCode;
    }

    String GetMessage(){
        return message;
    }
}
