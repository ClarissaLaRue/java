package ru.nsu.fit.korobova;

import java.io.InputStream;
import java.util.HashMap;

public class Request {
    private StringBuilder verb;
    private StringBuilder path;
    private StringBuilder protocol;
    private InputStream body;
    private HashMap<String, String> headers = new HashMap<>();

    private static final String GET = "GET";
    private static final String PROTOCOL = "HTTP/1.1";

    public void setVerb(String v) throws ServerException {
        if (!v.equals(GET)){
            throw new ServerException(405, "Method Not Allowed");
        }
        verb = new StringBuilder(v);
    }

    public void setPath(String p) throws ServerException {
        if ((p.lastIndexOf("\\") == p.length()-1) && (!p.equals("\\"))){//Если последний символ \
            throw new ServerException(404, "NotFound");
        }
        path = new StringBuilder(p);
    }

    public void setProtocol(String protocol) throws ServerException {
        if (!protocol.equals(PROTOCOL)) {
            throw  new ServerException(404, "NotFound");
        }
        this.protocol = new StringBuilder(protocol);
    }

    public void setBody(InputStream in){
        body = in;
    }

    public void setHeaders(String headers){
        String[] block = headers.split("\n");
        String head;
        String body;
        for (int i = 0; i < block.length; i++){
            head = block[i].substring(0, block[i].indexOf(":"));
            body = block[i].substring(block[i].indexOf(":")+1 , block[i].length());
            this.headers.put(head, body);
        }
    }

    StringBuilder getPath(){
        return path;
    }

    StringBuilder getProtocol(){
        return protocol;
    }
}
