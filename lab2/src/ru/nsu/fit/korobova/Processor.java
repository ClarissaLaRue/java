package ru.nsu.fit.korobova;

public interface Processor {
    Response process (Request request) throws ServerException;
}
