package ru.nsu.fit.korobova;

public class MyThread extends Thread {
    private final BlockingQueue<Runnable> queue;

    public MyThread(BlockingQueue<Runnable> queue){
        this.queue = queue;
    }

    @Override
    public void run(){
        while (!isInterrupted()){
            try {
                 Runnable task = queue.pop();
                 task.run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
