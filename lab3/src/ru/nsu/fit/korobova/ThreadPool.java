package ru.nsu.fit.korobova;

import java.util.ArrayList;

public class ThreadPool {
    private ArrayList <MyThread> threads;
    private BlockingQueue<Runnable> queue;

    public ThreadPool(int count){
        threads = new ArrayList<>(count);
        queue = new BlockingQueue<>(count);
        for (int i=0; i<count; i++){
             threads.add(new MyThread(queue));
        }
    }

    public void execute(Runnable task) throws InterruptedException {
        queue.add(task);
    }

    public void start(){
        threads.forEach(MyThread::start);
    }

    public void stop(){
        threads.forEach(MyThread::interrupt);
    }
}
