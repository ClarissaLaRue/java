package ru.nsu.fit.korobova;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        ThreadPool pool = new ThreadPool(3);
        pool.start();
        Runnable task1 = new Runnable() {
            @Override
            public void run() {
                System.out.print(" 1");
            }
        };
        Runnable task2 = new Runnable() {
            @Override
            public void run() {
                System.out.print(" 2");
            }
        };
        Runnable task3 = new Runnable() {
            @Override
            public void run() {
                System.out.print(" 3");
            }
        };
        try{
            pool.execute(task1);
            pool.execute(task3);
            pool.execute(task2);
        }catch (InterruptedException e) {
            System.out.print("OSHIBKA");
        }
        Thread.currentThread().sleep(5000);
    }
}
