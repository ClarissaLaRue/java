package ru.nsu.fit.korobova;

import java.util.LinkedList;
import java.util.Queue;

public class BlockingQueue<T> {
    private Queue<T> data;
    private int maxsize;

    private final Object object = new Object();

    public BlockingQueue(int count){
        data = new LinkedList<>();
        this.maxsize = count;
    }

    //Добавляем задачу в очередь
    public void add(T t) throws InterruptedException {
        synchronized (object){
            while (data.size() == maxsize) {//ждем, если очередь заполнена
                object.wait();
            }
            data.add(t);
            object.notify();
        }
    }

    //Достаем задачу из очереди
    public T pop() throws InterruptedException {
        synchronized (object){
            while (data.isEmpty()) {//ждем, если очередь пустая
                 object.wait();
            }
            T t = data.poll();
            object.notify();
            return t;
        }
    }
}
