package ru.nsu.fit.korobova;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;

public class ResponseSender {
    private Socket socket;
    private Response response;

    private static final String SuccessfulResponse = "OK";

    public void setSocket(Socket socket){
        this.socket = socket;
    }

    public void setResponse(Response response){
        this.response = response;
    }

    public void sendResponse() throws IOException {
        InputStream inputStream = new FileInputStream(response.getFile());
        OutputStream out = socket.getOutputStream();
        out.write(response.getProtocol().getBytes(Charset.forName("UTF-8")));
        out.write(" ".getBytes(Charset.forName("UTF-8")));
        out.write(SuccessfulResponse.getBytes(Charset.forName("UTF-8")));
        out.write("\n".getBytes(Charset.forName("UTF-8")));
        int c;
        while ((c = inputStream.read()) != -1){
            out.write((byte)c);
        }
        out.flush();
        socket.close();
    }
}
