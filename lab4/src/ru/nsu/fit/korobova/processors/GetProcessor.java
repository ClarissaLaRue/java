package ru.nsu.fit.korobova.processors;

import ru.nsu.fit.korobova.Request;
import ru.nsu.fit.korobova.Response;
import ru.nsu.fit.korobova.ServerException;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class GetProcessor implements Processor {
    private String directory = "./static";
    private String searchFile = "/index.html";

    @Override
    public Response process(Request request) throws ServerException {
        String path = new String(directory + request.getPath());
        File file;
        if (Files.isDirectory(Paths.get(path))){
            file = new File(path+searchFile);
        }else if (Files.isRegularFile(Paths.get(path))){
            file = new File(path);
        }else {
            throw new ServerException(404, "NotFound");
        }
        if (!file.exists()){
            throw new ServerException(404, "NotFound");
        }
        Response response = new Response();
        response.setProtocol(request.getProtocol());
        response.setFile(file);
        return response;
    }
}
