package ru.nsu.fit.korobova.processors;

import ru.nsu.fit.korobova.Request;
import ru.nsu.fit.korobova.Response;
import ru.nsu.fit.korobova.ServerException;

public interface Processor {
    Response process(Request request) throws ServerException;
}
