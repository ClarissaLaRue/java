package ru.nsu.fit.korobova;

import java.io.InputStream;
import java.util.HashMap;

public class Request {
    private StringBuilder verb;
    private StringBuilder path;
    private StringBuilder protocol;
    private InputStream body;
    private HashMap<String, String> headers = new HashMap<>();
    private HashMap<String, String> pathInfo = new HashMap<>();

    private static final String GET = "GET";
    private static final String PROTOCOL = "HTTP/1.1";

    public Request(){};

    public Request(Request request) {
        this.verb = request.getVerb();
        this.path = request.getPath();
        this.headers = request.getHeaders();
        this.body = request.getBody();
        this.protocol = request.getProtocol();
        this.pathInfo = request.getPathInfo();
    }

    public void fullPathInfo(String name, String segment){
        pathInfo.put(name, segment);
    }

    //Заполнение глагола
    public void setVerb(String v) throws ServerException {
        if (!v.equals(GET)){
            throw new ServerException(405, "Method Not Allowed");
        }
        verb = new StringBuilder(v);
    }

    //Заполнение пути
    public void setPath(String p) throws ServerException {
        if ((p.lastIndexOf("\\") == p.length()-1) && (!p.equals("\\"))){//Если последний символ \
            throw new ServerException(404, "NotFound");
        }
        path = new StringBuilder(p);
    }

    //Заполнение протокола
    public void setProtocol(String protocol) throws ServerException {
        if (!protocol.equals(PROTOCOL)) {
            throw  new ServerException(404, "NotFound");
        }
        this.protocol = new StringBuilder(protocol);
    }

    //Заполнение тела запроса
    public void setBody(InputStream in){
        body = in;
    }

    //Заполнение списка Headers
    public void setHeaders(String headers){
        String[] block = headers.split("\n");
        String head;
        String body;
        for (int i = 0; i < block.length; i++){
            head = block[i].substring(0, block[i].indexOf(":"));
            body = block[i].substring(block[i].indexOf(":")+1 , block[i].length());
            this.headers.put(head, body);
        }
    }

    public StringBuilder getVerb() {return verb;}

    public StringBuilder getPath(){
        return path;
    }

    public StringBuilder getProtocol(){
        return protocol;
    }

    public InputStream getBody() {return body; }

    public HashMap<String, String> getHeaders() {return headers; }

    public HashMap<String, String> getPathInfo() {return pathInfo; }
}
