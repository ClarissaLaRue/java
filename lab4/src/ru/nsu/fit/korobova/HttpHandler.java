package ru.nsu.fit.korobova;

public interface HttpHandler {
    Response handle (Request request);
}
