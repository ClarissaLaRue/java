package ru.nsu.fit.korobova;

import ru.nsu.fit.korobova.matchers.SegmentMatcher;
import ru.nsu.fit.korobova.processors.GetProcessor;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class Server {
    private int port;
    RequestHandler requestHandler = new RequestHandler();
    GetProcessor getProcessor = new GetProcessor();
    ResponseSender responseSender = new ResponseSender();
    MyTree tree = new MyTree();

    public Server(int p) {
        port = p;
    }

    //Регистрация обработчиков по пути
    public void register (List<SegmentMatcher> matchers, HttpHandler handler) throws ServerException, RegisterExeption {
        tree.addBranch(matchers, handler);
    }

    public void start () throws IOException, ServerException {
        ServerSocket serverSocket;
        try{
            serverSocket = new ServerSocket(port);
        }
        catch (IOException ex){
            throw new IOException();
        }
        Socket socket;

        while (true) {
            socket = serverSocket.accept();//Получаем Request
            InputStream in = socket.getInputStream();
            Request request = new Request();
            request = requestHandler.parseRequest(in); //Парсим Request
            Response response = new Response();//Получаем Response
            response = getProcessor.process(request);
            responseSender.setSocket(socket);
            responseSender.setResponse(response);
            responseSender.sendResponse();
        }

    }

    //Получение обработчика
    private HttpHandler getHendler(Request request){
        String segments[] = request.getPath().toString().split("/");
        int i=0;
        return tree.getHandlerForBranch(segments, request.getPathInfo());
    }

}
