package ru.nsu.fit.korobova;

import java.io.IOException;
import java.io.InputStream;

public class RequestHandler {

    public Request parseRequest(InputStream inputRequest) throws ServerException {
        int c = 0;
        int prev = c;
        Request request = new Request();
        StringBuilder requestStr = new StringBuilder();
        //Получаем строку запроса, без тела
        try{
            while (((c = inputRequest.read()) != -1 ) && ((((char)c != '\n'))||((char)prev != '\n'))){
                if (c == '\r') {
                    continue;
                }
                requestStr.append((char)c);
                prev = c;
            }
        } catch (IOException e) {
            throw new ServerException(500, "InternalServerError");
        }
        if (requestStr.length() != 0){
            request = parse(requestStr);
        }
        request.setBody(inputRequest);
        return request;
    }

    //Парсим запрос, без тела
    private Request parse(StringBuilder requestStr) throws ServerException {
        Request request = new Request();
        //Получаем глагол
        String verb = requestStr.substring(0, requestStr.indexOf(" "));
        request.setVerb(verb);
        requestStr = requestStr.delete(0, requestStr.indexOf(" ")+1);
        //Получаем путь
        String path = requestStr.substring(0, requestStr.indexOf(" "));
        request.setPath(path);
        requestStr = requestStr.delete(0, requestStr.indexOf(" ")+1);
        //Получаем протокол
        String protocol = requestStr.substring(0, requestStr.indexOf("\n"));
        request.setProtocol(protocol);
        requestStr = requestStr.delete(0, requestStr.indexOf("\n")+1);
        //Получаем список Headers
        request.setHeaders(new String(requestStr));
        return request;
    }
}
