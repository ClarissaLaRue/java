package ru.nsu.fit.korobova.matchers;

import java.util.Map;
import java.util.function.Predicate;

public class PredicateMatcher implements SegmentMatcher {
    private String name;
    Predicate<String> predicate;

    PredicateMatcher(Predicate<String> predicate, String name){
        this.name = name;
        this.predicate = predicate;
    }
    @Override
    public boolean match(String segment, Map<String, String> pathInfo) {
        if (name !=  null){
            if (predicate.test(segment)){
                pathInfo.put(name, segment);
                return true;
            }
        }
        return false;
    }
}
