package ru.nsu.fit.korobova.matchers;

import java.util.Map;

public class AnyMatcher implements SegmentMatcher {
    private String name;

    AnyMatcher(String name){
        this.name = name;
    }

    @Override
    public  boolean match(String segment, Map<String, String> pathInfo) {
        if (name != null){
            pathInfo.put(name, segment);
            return true;
        }
        return false;
    }
}
