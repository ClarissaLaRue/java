package ru.nsu.fit.korobova.matchers;

import java.util.Map;

public class StringMatcher implements SegmentMatcher {
    private String segment;
    private String name;

    StringMatcher(String segment, String name){
        this.segment = segment;
        this.name =  name;
    }

    @Override
    public boolean match(String segment, Map<String, String> pathInfo) {
        if (this.segment.equals(segment)){
            if (name != null){
                pathInfo.put(name, segment);
            }
            return true;
        }
        return false;
    }

}
