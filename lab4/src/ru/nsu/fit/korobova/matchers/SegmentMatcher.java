package ru.nsu.fit.korobova.matchers;

import java.util.Map;

public interface SegmentMatcher {
    public boolean match(String segment, Map<String, String> pathInfo);
}
