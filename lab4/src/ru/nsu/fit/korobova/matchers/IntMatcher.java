package ru.nsu.fit.korobova.matchers;

import java.util.Map;

public class IntMatcher implements SegmentMatcher {
    private String name;

    IntMatcher(String name){
        this.name = name;
    }
    @Override
    public boolean match(String segment, Map<String, String> pathInfo) {
        try {
            Integer  number = Integer.valueOf(segment);
        }
        catch (NumberFormatException e){
            return false;
        }
        pathInfo.put(name, segment);
        return true;
    }
}
