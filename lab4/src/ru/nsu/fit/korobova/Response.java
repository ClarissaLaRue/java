package ru.nsu.fit.korobova;

import java.io.File;
import java.util.HashMap;

public class Response {
    private StringBuilder protocol;
    private HashMap<String, String> headers;
    private File file;

    public void setProtocol(StringBuilder protocol){
        this.protocol = protocol;
    }

    public void setFile(File file){
        this.file = file;
    }

    String getProtocol(){
        return new String(protocol);
    }

    File getFile(){
        return file;
    }

}
