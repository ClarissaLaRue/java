package ru.nsu.fit.korobova;

public class ServerException extends Exception {
    private int errorCode;
    private String message;

    public ServerException(int code, String mes){
        errorCode = code;
        message = mes;
    }

    public int GetErrorCode(){
        return errorCode;
    }

    public String GetMessage(){
        return message;
    }
}
