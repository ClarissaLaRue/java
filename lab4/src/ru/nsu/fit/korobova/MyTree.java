package ru.nsu.fit.korobova;

import ru.nsu.fit.korobova.matchers.SegmentMatcher;

import java.util.Map;
import java.util.List;

public class MyTree {
    private Node root = null;

    public void addBranch(List<SegmentMatcher> matchers, HttpHandler httpHandler) throws RegisterExeption {
        if (matchers.isEmpty()){
            return;
        }
        //Добавлем root
        if (root == null){
            root = new Node(matchers.get(0));//если лист
        }else{
            if (!root.equals(new Node(matchers.get(0)))){//Если корень не совпадает с
                throw new RegisterExeption("Register error");
            }
        }
        Node parent = new Node(root.getMatcher());
        Node child;
        //Добавляем узлы до предпоследнего
        for (int i = 1; i < matchers.size()-1 ; i++){
            child = new Node(matchers.get(i));
            if (!parent.haveChild(child)){
                parent.addChild(child);
            }
            parent = child;
        }
        //Последний остается лист
        child = new Node(matchers.get(matchers.size()-1), httpHandler);
        parent.addChild(child);
    }

    //Получение обработчика по пути
    public HttpHandler getHandlerForBranch(String[] segments, Map<String, String> pathInfo){
        if (root == null){
            return null;
        }
        boolean flag = true;
        Node current = root;
        Node child;
        int counterForNode=0;
        int counterForChild=0;
        while(true){
            flag = current.getMatcher().match(segments[counterForNode], pathInfo);
            if (!flag){
                return null;
            }
            if (current.isLeaf()) {//Дошли листа
                if (counterForNode == (segments.length-1)){//Проверили весь путь
                    return current.getHandler();
                }else {//Дошли до листа, но путь еще не закончился
                    return null;
                }
            }
            counterForNode++;
            if (counterForNode >= segments.length-1){
                return null;
            }
            flag = false;
            while( (counterForChild<current.numberOfChild()) && (!flag) ){ //Проверяем детей, пока не закончатся или не найдем нужного
                flag = current.getChild(counterForChild).getMatcher().match(segments[counterForNode], pathInfo);
                counterForChild++;
            }
            if ((counterForChild == current.numberOfChild()) && (!flag)){//Среди детей нет нужного
                return null;
            }
            current = current.getChild(counterForChild);
        }
    }
}
