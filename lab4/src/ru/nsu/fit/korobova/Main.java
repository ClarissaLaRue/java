package ru.nsu.fit.korobova;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Server server = new Server(8000);
        try {
            server.start();
        } catch (ServerException e) {
            String message = new String(String.valueOf(e.GetErrorCode()) + " " + e.GetMessage() );
            System.out.print(message);
        } catch (IOException e) {
            e.fillInStackTrace();
            //System.out.print(e.ge);
        }
    }
}
