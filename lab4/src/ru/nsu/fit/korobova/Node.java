package ru.nsu.fit.korobova;

import ru.nsu.fit.korobova.matchers.SegmentMatcher;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private List<Node> children = new ArrayList<Node>();
    private SegmentMatcher matcher;
    private HttpHandler handler;
    private boolean isLeaf;

    //Если узел, то заполняет только matcher
    public Node(SegmentMatcher matcher) {
        this.matcher = matcher;
        isLeaf = false;
    }

    //Если лист, то заполняем matcher и Handler
    public Node(SegmentMatcher matcher, HttpHandler httpHandler) {
        this.matcher = matcher;
        this.handler = httpHandler;
        isLeaf = true;
    }

    //Добавляем ребенка
    public void addChild(Node child) {
        children.add(child);
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public SegmentMatcher getMatcher() {
        return matcher;
    }

    public HttpHandler getHandler(){
        return handler;
    }

    public boolean haveChild(Node node) {
        for (int i = 0; i < children.size(); i++) {
            if (node.equals(children.get(i))) {
                return true;
            }
        }
        return false;
    }

    public int numberOfChild() {
        return children.size();
    }

    public Node getChild(int number) {
        if (number < numberOfChild()) {
            return children.get(number);
        }
        return null;
    }
}
